import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import VueTextAreaAutoSize from "vue-textarea-autosize";
import firebase from "firebase/app";
import "firebase/firestore";

Vue.use(VueTextAreaAutoSize);

Vue.config.productionTip = false;

firebase.initializeApp({
  apiKey: "AIzaSyCKB3SG7zvc_ERPMbBBnYzoM3sNeaiSW84",
  authDomain: "vuetify-calendar-91f07.firebaseapp.com",
  projectId: "vuetify-calendar-91f07",
  storageBucket: "vuetify-calendar-91f07.appspot.com",
  messagingSenderId: "70817212684",
  appId: "1:70817212684:web:e8e93390f54f1baaeac129",
});

export const db = firebase.firestore();

new Vue({
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
